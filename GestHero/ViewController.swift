import UIKit

enum Gestures : String {
    case tap = "Tap"
    case longPress = "Long Press"
    case swipeDown = "Swipe Down"
    case swipeUp = "Swipe Up"
    case swipeLeft = "Swipe Left"
    case swipeRight = "Swipe Right"
    case pinch = "Pinch"
    case tap2 = "Tap 2 Fingers"
    case tap3 = "Tap 3 Fingers"
    case shake = "Shake"
    case none = "Unsuported"
    
    static func allCases() -> [Gestures] {
        return [.tap, .longPress, .swipeDown, .swipeUp, .swipeLeft, .swipeRight, .pinch, .tap2, .tap3, .shake]
    }
    
    static func randomCase(_ relaxMode: Bool) -> Gestures {
        let allGestures = Gestures.allCases()
        let count = relaxMode ? 6 : UInt32(allGestures.count)
        let randomIndex = Int(arc4random_uniform(count))
        return allGestures[randomIndex]
    }
    
}

class ViewController: UIViewController {
    var commandIndex = Gestures.tap
    var lastGesture = Gestures.none
    var isPlaying = false
    var remainingMoveTime = 70 // move time remaining during move
    var score = 0
    var highScore = 0
    var relaxMode = true
    
    @IBOutlet weak var commandLabel: UILabel!
    @IBOutlet weak var remainingTimeLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    
    weak var delegate: ScoreDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        // hide commandLabel but don't hide button
        commandLabel.text = "GestHero!"
        
        // set-up remaining time label
        remainingTimeLabel.text = "Time: "+String(Double(remainingMoveTime)/10.0) + "s"
        scoreLabel.text = "Score: 0"
        
        // listen for taps, pinches, and swipes
        var tapGesture = UITapGestureRecognizer(target: self, action: #selector (self.touchAction (_:)))
        self.view.addGestureRecognizer(tapGesture)
        
        // listen for taps, pinches, and swipes
        tapGesture = UITapGestureRecognizer(target: self, action: #selector (self.touchAction (_:)))
        tapGesture.numberOfTouchesRequired = 2
        self.view.addGestureRecognizer(tapGesture)
        
        // listen for taps, pinches, and swipes
        tapGesture = UITapGestureRecognizer(target: self, action: #selector (self.touchAction (_:)))
        tapGesture.numberOfTouchesRequired = 3
        self.view.addGestureRecognizer(tapGesture)
        
        let pinchGesture = UIPinchGestureRecognizer(target: self, action: #selector (self.pinchAction (_:)))
        self.view.addGestureRecognizer(pinchGesture)
        
        let longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector (self.longPressAction(_:)))
        longPressGesture.minimumPressDuration = 0.2
        self.view.addGestureRecognizer(longPressGesture)
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(swipeAction(_:)))
        swipeLeft.direction = .left
        self.view.addGestureRecognizer(swipeLeft)
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(swipeAction(_:)))
        swipeRight.direction = .right
        self.view.addGestureRecognizer(swipeRight)
        
        let swipeUp = UISwipeGestureRecognizer(target: self, action: #selector(swipeAction(_:)))
        swipeUp.direction = .up
        self.view.addGestureRecognizer(swipeUp)
        
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(swipeAction(_:)))
        swipeDown.direction = .down
        self.view.addGestureRecognizer(swipeDown)
        
        startGame()
        
    }
    
    func startGame() {
        // schedule function in background to run down remaining move time
        Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.update), userInfo: nil, repeats: true)
        
        // determine command
        commandIndex = Gestures.randomCase(relaxMode)
        
        // update command Label
        updateInterface()
        
        // turn on game mode
        isPlaying = true
    }
    
    func updateInterface() {
        commandLabel.text = commandIndex.rawValue
        scoreLabel.text = "Score: \(score)"
        remainingTimeLabel.text = "Time: "+String(Double(remainingMoveTime)/10.0) + "s"
    }
    
    override var canBecomeFirstResponder: Bool {
        get {
            return true
        }
    }
    
    @objc func touchAction(_ sender:UITapGestureRecognizer){
        NSLog("Tap!")
        
        // if game mode, check if it was a good move
        if isPlaying {
            if (commandIndex == .tap)
            || (commandIndex == .tap2 && sender.numberOfTouchesRequired == 2)
                || (commandIndex == .tap3 && sender.numberOfTouchesRequired == 3){
                goodMove()
            } else {
                badMove()
            }
        }
    }
    
    @objc func longPressAction(_ sender:UILongPressGestureRecognizer){
        NSLog("Long Press")
        
        // if game mode, check if it was a good move
        if isPlaying {
            if commandIndex == .longPress {
                goodMove()
            } else if commandIndex != .longPress && sender.state != .ended {
                badMove()
            }
        }
    }
    
    @objc func swipeAction(_ sender:UISwipeGestureRecognizer){
        NSLog("Swipe" + String(sender.direction.rawValue))
        
        // if game mode, check if it was a good move
        if isPlaying {
            if (commandIndex == .swipeDown && sender.direction == .down)
            || (commandIndex == .swipeUp && sender.direction == .up)
            || (commandIndex == .swipeLeft && sender.direction == .left)
            || (commandIndex == .swipeRight && sender.direction == .right){
                goodMove()
            } else {
                badMove()
            }
        }
    }
    
    override func motionEnded(_ motion: UIEventSubtype, with event: UIEvent?) {
        NSLog("Shake")
        
        // if game mode, check if it was a good move
        if isPlaying {
            if commandIndex == .shake && motion == .motionShake {
                goodMove()
            } else {
                badMove()
            }
        }
    }
    
    @objc func pinchAction(_ sender:UIPinchGestureRecognizer){
        NSLog("Pinch")
        
        // if game mode, check if it was a good move
        if isPlaying {
            if commandIndex == .pinch {
                goodMove()
            } else if commandIndex != .pinch
                        && sender.state == .ended
                        && lastGesture != .pinch {
                badMove()
            }
        }
    }
    
    func goodMove() {
        UIView.animate(withDuration: 0.5, animations: {
            self.view.backgroundColor = UIColor(hex: 0xC8FFC8)
        })
        UIView.animate(withDuration: 0.5, animations: {
            self.view.backgroundColor = UIColor.white
        })
        
        // find new command that is not equal to the current command
        lastGesture = commandIndex
        while(lastGesture == commandIndex) {
            commandIndex = Gestures.randomCase(relaxMode)
        }
        
        if commandIndex == .pinch || commandIndex == .longPress {
            remainingMoveTime += 5
        } else if commandIndex == .shake {
            remainingMoveTime += 10
        } else {
            remainingMoveTime += 3            
        }
        
        score += 1
        
        // update the label
        updateInterface()
    }
    
    func badMove() {
        UIView.animate(withDuration: 0.5, animations: {
            self.view.backgroundColor = UIColor(hex: 0xFF8787)
        })
        UIView.animate(withDuration: 0.5, animations: {
            self.view.backgroundColor = UIColor.white
        })
        
        score -= 2
        
        if score < 0 {
            score = 0
        }
        
        // update the label
        updateInterface()
    }
    
    func endGame(_ negativeScore: Bool = false) {
        // turn off game mode
        isPlaying = false
        
        // adjust label and button visibility
        if score > highScore {
            highScore = score
        }
        
        remainingTimeLabel.isHidden = true
        scoreLabel.isHidden = true
        
        self.delegate.updateScore(highScore: highScore, lastScore: score)
        
        commandLabel.text = "GestHero!"
        
        let lostGameMessage = "I'm sorry, bro ;(\nScore: \(score)"
        
        let alert = UIAlertController(title: negativeScore ? "Negative score..." : "Time is over...", message: lostGameMessage, preferredStyle: .alert)
        let okButton = UIAlertAction(title: "OK", style: .default) { (action: UIAlertAction) in
            self.dismiss(animated: true, completion: nil)
        }
        
        alert.addAction(okButton)
        self.present(alert, animated: true)
        
        score = 0
    }
    
    @objc func update() {
        // only decrement the timer during game mode
        if isPlaying {
            remainingMoveTime -= 1
            updateInterface()
            
            // the game is over if the player runs out of time
            if remainingMoveTime < 0 {
                endGame()
            }
        }
    }
}
