//
//  MultiPlayerViewController.swift
//  GestHero
//
//  Created by Ada 2018 on 26/07/2018.
//  Copyright © 2018 Academy. All rights reserved.
//

import UIKit

class MultiPlayerViewController: UIViewController {

    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var player1Label: UILabel!
    @IBOutlet weak var player2Label: UILabel!
    @IBOutlet weak var commandLabel1: UILabel!
    @IBOutlet weak var commandLabel2: UILabel!
    @IBOutlet weak var scoreLabel1: UILabel!
    @IBOutlet weak var scoreLabel2: UILabel!
    @IBOutlet weak var timerLabel1: UILabel!
    @IBOutlet weak var timerLabel2: UILabel!
    @IBOutlet weak var startGameButton: MainUIButton!
    @IBOutlet weak var backButton: UIButton!
    
    var isPlaying = false
    var remainingMoveTime = 70 // move time remaining during move
    var score1 = 0
    var score2 = 0
    
    var commands: [Gestures] = []
    var index1 = 0
    var index2 = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view2.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
        player2Label.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
        
        timerLabel1.isHidden = true
        scoreLabel1.isHidden = true
        timerLabel2.isHidden = true
        scoreLabel2.isHidden = true
        
        setupGestures()
        
        // schedule function in background to run down remaining move time
        Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.update), userInfo: nil, repeats: true);
    }
    
    func setupGestures() {
        // listen for taps, pinches, and swipes
        var tapGesture = UITapGestureRecognizer(target: self, action: #selector (self.touchAction(_:)))
        self.view1.addGestureRecognizer(tapGesture)
        
        tapGesture = UITapGestureRecognizer(target: self, action: #selector (self.touchAction(_:)))
        self.view2.addGestureRecognizer(tapGesture)
        
        // listen for taps, pinches, and swipes
        tapGesture = UITapGestureRecognizer(target: self, action: #selector (self.touchAction (_:)))
        tapGesture.numberOfTouchesRequired = 2
        self.view1.addGestureRecognizer(tapGesture)
        
        // listen for taps, pinches, and swipes
        tapGesture = UITapGestureRecognizer(target: self, action: #selector (self.touchAction (_:)))
        tapGesture.numberOfTouchesRequired = 2
        self.view2.addGestureRecognizer(tapGesture)
        
        // listen for taps, pinches, and swipes
        tapGesture = UITapGestureRecognizer(target: self, action: #selector (self.touchAction (_:)))
        tapGesture.numberOfTouchesRequired = 3
        self.view1.addGestureRecognizer(tapGesture)
        
        // listen for taps, pinches, and swipes
        tapGesture = UITapGestureRecognizer(target: self, action: #selector (self.touchAction (_:)))
        tapGesture.numberOfTouchesRequired = 3
        self.view2.addGestureRecognizer(tapGesture)
        
        var pinchGesture = UIPinchGestureRecognizer(target: self, action: #selector (self.pinchAction(_:)))
        self.view1.addGestureRecognizer(pinchGesture)
        
        pinchGesture = UIPinchGestureRecognizer(target: self, action: #selector (self.pinchAction(_:)))
        self.view2.addGestureRecognizer(pinchGesture)
        
        var longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector (self.longPressAction(_:)))
        longPressGesture.minimumPressDuration = 0.2
        self.view1.addGestureRecognizer(longPressGesture)
        
        longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector (self.longPressAction(_:)))
        longPressGesture.minimumPressDuration = 0.2
        self.view2.addGestureRecognizer(longPressGesture)
        
        var swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(swipeAction(_:)))
        swipeLeft.direction = .left
        self.view1.addGestureRecognizer(swipeLeft)
        
        swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(swipeAction(_:)))
        swipeLeft.direction = .left
        self.view2.addGestureRecognizer(swipeLeft)
        
        var swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(swipeAction(_:)))
        swipeRight.direction = .right
        self.view1.addGestureRecognizer(swipeRight)
        
        swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(swipeAction(_:)))
        swipeRight.direction = .right
        self.view2.addGestureRecognizer(swipeRight)
        
        var swipeUp = UISwipeGestureRecognizer(target: self, action: #selector(swipeAction(_:)))
        swipeUp.direction = .up
        self.view1.addGestureRecognizer(swipeUp)
        
        swipeUp = UISwipeGestureRecognizer(target: self, action: #selector(swipeAction(_:)))
        swipeUp.direction = .up
        self.view2.addGestureRecognizer(swipeUp)
        
        var swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(swipeAction(_:)))
        swipeDown.direction = .down
        self.view1.addGestureRecognizer(swipeDown)
        
        swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(swipeAction(_:)))
        swipeDown.direction = .down
        self.view2.addGestureRecognizer(swipeDown)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func startGame(_ sender: UIButton) {
        self.remainingMoveTime = 70
        startGameButton.isHidden = true
        backButton.isHidden = true
        
        timerLabel1.isHidden = false
        scoreLabel1.isHidden = false
        timerLabel2.isHidden = false
        scoreLabel2.isHidden = false
        player1Label.isHidden = false
        player2Label.isHidden = false
        
        // determine command
        commands.append(Gestures.tap)
        
        // update command Label
        updateInterface()
        
        // turn on game mode
        isPlaying = true
    }
    
    @IBAction func didPressBack(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func updateInterface() {
        commandLabel1.text = commands[index1].rawValue
        commandLabel2.text = commands[index2].rawValue
        
        scoreLabel1.text = "Score: \(score1)"
        scoreLabel2.text = "Score: \(score2)"
        
        timerLabel1.text = "Time: "+String(Double(remainingMoveTime)/10.0) + "s"
        timerLabel2.text = "Time: "+String(Double(remainingMoveTime)/10.0) + "s"
    }
    
    @objc func touchAction(_ sender:UITapGestureRecognizer){
        NSLog("Tap!")
        
        // if game mode, check if it was a good move
        if isPlaying {
            if sender.view == view1 {
                if (commands[index1] == .tap)
                    || (commands[index1] == .tap2 && sender.numberOfTouchesRequired == 2)
                    || (commands[index1] == .tap3 && sender.numberOfTouchesRequired == 3) {
                    goodMove(true)
                } else {
                    badMove(true)
                }
            } else {
                if (commands[index2] == .tap)
                    || (commands[index2] == .tap2 && sender.numberOfTouchesRequired == 2)
                    || (commands[index2] == .tap3 && sender.numberOfTouchesRequired == 3) {
                    goodMove(false)
                } else {
                    badMove(false)
                }
            }
        }
    }
    
    @objc func longPressAction(_ sender:UIPinchGestureRecognizer){
        NSLog("Long Press")
        
        // if game mode, check if it was a good move
        if isPlaying {
            if sender.view == view1 {
                if commands[index1] == .longPress {
                    goodMove(true)
                } else if commands[index1] != .longPress && sender.state != .ended {
                    badMove(true)
                }
            } else {
                if commands[index2] == .longPress {
                    goodMove(false)
                } else if commands[index2] != .longPress && sender.state != .ended {
                    badMove(false)
                }
            }
        }
    }
    
    @objc func swipeAction(_ sender:UISwipeGestureRecognizer){
        NSLog("Swipe" + String(sender.direction.rawValue))
        
        // if game mode, check if it was a good move
        if isPlaying {
            if sender.view == view1 {
                if (commands[index1] == .swipeDown && sender.direction == .down)
                    || (commands[index1] == .swipeUp && sender.direction == .up)
                    || (commands[index1] == .swipeLeft && sender.direction == .left)
                    || (commands[index1] == .swipeRight && sender.direction == .right){
                    goodMove(true)
                } else {
                    badMove(true)
                }
            } else {
                if (commands[index2] == .swipeDown && sender.direction == .down)
                    || (commands[index2] == .swipeUp && sender.direction == .up)
                    || (commands[index2] == .swipeLeft && sender.direction == .left)
                    || (commands[index2] == .swipeRight && sender.direction == .right){
                    goodMove(false)
                } else {
                    badMove(false)
                }
            }
        }
    }
    
    @objc func pinchAction(_ sender:UIPinchGestureRecognizer){
        NSLog("Pinch")
        
        // if game mode, check if it was a good move
        if isPlaying {
            if sender.view == view1 {
                if commands[index1] == .pinch {
                    goodMove(true)
                } else if commands[index1] != .pinch
                        && sender.state == .ended
                        && commands[index1 - 1] != .pinch {
                    badMove(true)
                }
            } else {
                if commands[index2] == .pinch {
                    goodMove(false)
                } else if commands[index2] != .pinch
                        && sender.state == .ended
                        && commands[index2 - 1] != .pinch {
                    badMove(false)
                }
            }
        }
    }
    
    func goodMove(_ isPlayer1: Bool) {
        if isPlayer1 {
            UIView.animate(withDuration: 0.5, animations: {
                self.view1.backgroundColor = UIColor(hex: 0xC8FFC8)
            })
            UIView.animate(withDuration: 0.5, animations: {
                self.view1.backgroundColor = UIColor.white
            })
        
            score1 += 1
            index1 += 1
        } else {
            UIView.animate(withDuration: 0.5, animations: {
                self.view2.backgroundColor = UIColor(hex: 0xC8FFC8)
            })
            UIView.animate(withDuration: 0.5, animations: {
                self.view2.backgroundColor = UIColor.white
            })
            
            score2 += 1
            index2 += 1
        }
        
        if (isPlayer1 && index1 == commands.count) || (!isPlayer1 && index2 == commands.count) {
            // find new command that is not equal to the current command
            var sorted = commands.last!
            while(sorted == commands.last! || sorted == .shake) {
                sorted = Gestures.randomCase(false)
            }
            
            commands.append(sorted)
            
            if sorted == .pinch || sorted == .longPress {
                remainingMoveTime += 5
            } else if sorted == .shake {
                remainingMoveTime += 10
            } else {
                remainingMoveTime += 3
            }
        }
        
        // update the label
        updateInterface()
    }
    
    func badMove(_ isPlayer1: Bool) {
        if isPlayer1 {
            UIView.animate(withDuration: 0.5, animations: {
                self.view1.backgroundColor = UIColor(hex: 0xFF8787)
            })
            UIView.animate(withDuration: 0.5, animations: {
                self.view1.backgroundColor = UIColor.white
            })
            
            score1 -= 2
        } else {
            UIView.animate(withDuration: 0.5, animations: {
                self.view2.backgroundColor = UIColor(hex: 0xFF8787)
            })
            UIView.animate(withDuration: 0.5, animations: {
                self.view2.backgroundColor = UIColor.white
            })
            
            score2 -= 2
        }
        
        if score1 < 0 {
            score1 = 0
        }
            
        if score2 < 0 {
            score2 = 0
        }
        
        // update the label
        updateInterface()
    }
    
    func endGame(_ negativeScore: Bool = false) {
        // turn off game mode
        isPlaying = false
        
        timerLabel1.isHidden = true
        scoreLabel1.isHidden = true
        timerLabel2.isHidden = true
        scoreLabel2.isHidden = true
        
        commandLabel1.text = "READY?"
        commandLabel2.text = "READY?"
        
        self.backButton.isHidden = false
        self.startGameButton.isHidden = false
        
        var lostGameMessage = ""
        if score1 > score2 {
            lostGameMessage = "Player 1 won!\n\(score1) - \(score2)"
        } else if score1 < score2 {
            lostGameMessage = "Player 2 won!\n\(score1) - \(score2)"
        } else {
            lostGameMessage = "Draw!!\n\(score1) - \(score2)"
        }
        
        self.score1 = 0
        self.score2 = 0
        self.index1 = 0
        self.index2 = 0
        self.commands = [Gestures.tap]
        
        let alert = UIAlertController(title: negativeScore ? "Negative score..." : "Time is over...", message: lostGameMessage, preferredStyle: .alert)
        let okButton = UIAlertAction(title: "OK", style: .default, handler: nil)
        
        alert.addAction(okButton)
        self.present(alert, animated: true)
    }
    
    @objc func update() {
        // only decrement the timer during game mode
        if isPlaying {
            remainingMoveTime -= 1
            updateInterface()
            
            // the game is over if the player runs out of time
            if remainingMoveTime < 0 {
                endGame()
            }
        }
    }
}
