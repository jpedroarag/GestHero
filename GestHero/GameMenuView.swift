//
//  GameMenuViewController.swift
//  GestHero
//
//  Created by Ada 2018 on 25/07/2018.
//  Copyright © 2018 Academy. All rights reserved.
//

import UIKit

class GameMenuView: UIView {

    @IBOutlet weak var gameModeButton: UIButton!
    @IBOutlet weak var highScoreLabel: UILabel!
    @IBOutlet weak var lastScoreLabel: UILabel!
    
    var relaxMode = true
    
    // delegate para inicializar o jogo, de acordo com o menu correspondente
    weak var delegate: SinglePlayerDelegate!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    // Ação do botão gameModeButton
    @IBAction func startGame(_ sender: UIButton) {
        self.delegate.startGameWithMode(relaxMode: self.relaxMode)
    }
}
