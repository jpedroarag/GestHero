//
//  SinglePlayerDelegate.swift
//  GestHero
//
//  Created by Ada 2018 on 25/07/2018.
//  Copyright © 2018 Academy. All rights reserved.
//

import Foundation

protocol SinglePlayerDelegate: class {
    func startGameWithMode(relaxMode: Bool)
}
