//
//  SinglePlayerViewController.swift
//  GestHero
//
//  Created by Ada 2018 on 25/07/2018.
//  Copyright © 2018 Academy. All rights reserved.
//

import UIKit

class SinglePlayerViewController: UIViewController, SinglePlayerDelegate, ScoreDelegate {
    
    @IBOutlet weak var menuScrollView: UIScrollView!
    @IBOutlet weak var menuPageControl: UIPageControl!
    
    // Views de menu para cada modo de jogo (relax e hard)
    var relaxModeView: GameMenuView!
    var hardModeView: GameMenuView!

    // Flag para indicar o modo de jogo em que se iniciará
    var relaxMode = true

    // Variáveis para guardar os scores do jogador
    var highScoreRelax = 0
    var highScoreHard = 0
    var lastScoreRelax = 0
    var lastScoreHard = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        highScoreRelax = UserDefaults.standard.integer(forKey: "highScoreRelax")
        lastScoreRelax = UserDefaults.standard.integer(forKey: "lastScoreRelax")
        highScoreHard = UserDefaults.standard.integer(forKey: "highScoreHard")
        lastScoreHard = UserDefaults.standard.integer(forKey: "lastScoreHard")
        
        menuScrollView.delegate = self
        
        relaxModeView = Bundle.main.loadNibNamed("GameMenuView", owner: self, options: nil)?.first as! GameMenuView
        relaxModeView.gameModeButton.setTitle("Relax Mode", for: .normal)
        relaxModeView.highScoreLabel.text = "High Score: \(highScoreRelax)"
        relaxModeView.lastScoreLabel.text = "Last Score: \(lastScoreRelax)"
        relaxModeView.relaxMode = true
        relaxModeView.delegate = self
        
        menuScrollView.addSubview(relaxModeView)
        
        hardModeView = Bundle.main.loadNibNamed("GameMenuView", owner: self, options: nil)?.first as! GameMenuView
        hardModeView.gameModeButton.setTitle("Hard Mode", for: .normal)
        hardModeView.highScoreLabel.text = "High Score: \(highScoreHard)"
        hardModeView.lastScoreLabel.text = "Last Score: \(lastScoreHard)"
        hardModeView.relaxMode = false
        hardModeView.delegate = self
        
        menuScrollView.addSubview(hardModeView)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        relaxModeView.frame = menuScrollView.frame
        relaxModeView.frame.origin = CGPoint(x: 0, y: 0)
        
        hardModeView.frame = CGRect(origin: CGPoint(x: menuScrollView.frame.width, y: 0), size: menuScrollView.frame.size)
        
        menuScrollView.contentSize = CGSize(width: self.view.bounds.size.width * 2, height: menuScrollView.frame.height)
    }
    
    // Ação do botão de voltar
    @IBAction func dismiss(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    // Ação de toque no Page Control - "scrolla" a scroll view horizontalmente para o outro menu
    @IBAction func changeScreen(_ sender: UIPageControl) {
        menuScrollView.scrollRectToVisible(CGRect(origin: CGPoint(x: menuScrollView.frame.width * CGFloat (sender.currentPage), y: 0), size: menuScrollView.frame.size), animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let dest = segue.destination as! ViewController
        dest.relaxMode = self.relaxMode
        dest.delegate = self
        
        dest.highScore = self.relaxMode ? highScoreRelax : highScoreHard
    }
    
    // Inicia o jogo
    func startGameWithMode(relaxMode: Bool) {
        self.relaxMode = relaxMode
        performSegue(withIdentifier: "startGameSegue", sender: self)
    }
    
    // Atualiza as pontuações (delegate)
    func updateScore(highScore: Int, lastScore: Int) {
        // Pega a View de menu a partir do modo que acabou de ser jogado
        let v = menuScrollView.subviews[relaxMode ? 0 : 1] as! GameMenuView
        
        // Atualiza os valores de pontuação da SinglePlayerViewController
        highScoreRelax = relaxMode ? highScore : highScoreRelax
        highScoreHard = !relaxMode ? highScore : highScoreHard
        
        // Atualiza os labels
        v.highScoreLabel.text = "High Score: \(highScore)"
        v.lastScoreLabel.text = "Last Score: \(lastScore)"
        
        if relaxMode {
            UserDefaults.standard.setValue(highScore, forKey: "highScoreRelax")
            UserDefaults.standard.setValue(lastScore, forKey: "lastScoreRelax")
        } else {
            UserDefaults.standard.setValue(highScore, forKey: "highScoreHard")
            UserDefaults.standard.setValue(lastScore, forKey: "lastScoreHard")
        }
        
    }
}

extension SinglePlayerViewController: UIScrollViewDelegate {
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        // Atualiza o Page Control
        let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
        menuPageControl.currentPage = Int(pageNumber)
    }
}
