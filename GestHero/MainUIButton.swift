//
//  MainUIButton.swift
//  GestHero
//
//  Created by Ada 2018 on 26/07/2018.
//  Copyright © 2018 Academy. All rights reserved.
//

import UIKit

// Classe criada para padronizar os botões da aplicação
class MainUIButton: UIButton {


    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        backgroundColor = UIColor(hex: 0x65EBEB)
        layer.masksToBounds = true
        layer.cornerRadius = 4
    }


}
